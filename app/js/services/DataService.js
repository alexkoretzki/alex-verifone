(function () {
    'use strict';

    angular.module('verifone')
       
        .factory('DataService', function () {
            var data = [
                {'id':'1','name':'alex','grade':'85','email':'www@www.com','date_joined':'10/3/17','address':'tabenkin','city':'ramat gan','country':'israel','zip':'1111','subject':'math'},
                {'id':'2','name':'alex','grade':'85','email':'www@www.com','date_joined':'10/3/17','address':'tabenkin','city':'ramat gan','country':'israel','zip':'1111','subject':'math'},
                {'id':'3','name':'alex','grade':'85','email':'www@www.com','date_joined':'10/3/17','address':'tabenkin','city':'ramat gan','country':'israel','zip':'1111','subject':'math'},
                {'id':'4','name':'alex','grade':'85','email':'www@www.com','date_joined':'10/3/17','address':'tabenkin','city':'ramat gan','country':'israel','zip':'1111','subject':'math'},
                {'id':'5','name':'alex','grade':'85','email':'www@www.com','date_joined':'10/3/17','address':'tabenkin','city':'ramat gan','country':'israel','zip':'1111','subject':'math'},
                {'id':'6','name':'alex','grade':'85','email':'www@www.com','date_joined':'10/3/17','address':'tabenkin','city':'ramat gan','country':'israel','zip':'1111','subject':'math'},
                {'id':'7','name':'alex','grade':'85','email':'www@www.com','date_joined':'10/3/17','address':'tabenkin','city':'ramat gan','country':'israel','zip':'1111','subject':'math'},
                {'id':'8','name':'alex','grade':'85','email':'www@www.com','date_joined':'10/3/17','address':'tabenkin','city':'ramat gan','country':'israel','zip':'1111','subject':'math'},
                {'id':'9','name':'alex','grade':'85','email':'www@www.com','date_joined':'10/3/17','address':'tabenkin','city':'ramat gan','country':'israel','zip':'1111','subject':'math'},
                {'id':'10','name':'alex','grade':'85','email':'www@www.com','date_joined':'10/3/17','address':'tabenkin','city':'ramat gan','country':'israel','zip':'1111','subject':'math'}
            ];
            return {
                //get the data
              getData :function(){
                return data;
              },
              //save data
              updateData: function(newData){
                this.data = newData;
              },
              //check if id exist
              checkIfIdExist : function(id){
                for(var i=0; i<data.length;i++){
                    if(data[i].id == id){
                        alert('the id already exist, choose different id');
                        return false;
                    }
                    return true;
                }
              } 
                    
            };
                
        });
})();

