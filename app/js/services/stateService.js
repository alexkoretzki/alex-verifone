(function () {
    'use strict';

    angular.module('verifone')
       
        .factory('stateService', function () {
           var state = {};
            return {
              getState :function(){
                return this.state;
              },
              updateState: function(state){
                this.state = state;
              } 
                    
            };
                
        });
})();

