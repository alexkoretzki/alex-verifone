(function () {
    'use strict';

    angular.module('verifone')
    .controller('dataController', ['DataService','getDataFromService','$rootScope','stateService','getState','$scope','$filter',
        function(DataService,getDataFromService,$rootScope,stateService,getState,$scope,$filter) {
            var vm = this;
            var current_state = {};
            var selectedRowIndex = undefined;
            vm.filterInput = {};
            init();
            //selected row, update the view with the correct row details
            vm.selectRow = function(index){
                vm.rowSelected = true;
                selectedRowIndex = index;
                vm.rowData =  vm.data[index];
            }
            vm.addData = function(){
                vm.rowSelected = false;
                vm.rowData = {};
                vm.newData = true;
            }
            //remove row and update the data in service
            vm.remove = function(){
                if(!angular.isUndefined(selectedRowIndex)){
                    vm.data.splice(selectedRowIndex,1);
                    //selectedRowIndex = undefined;
                    vm.rowSelected = false;
                    DataService.updateData(vm.data);
                }
            }
            //add the new object to the view array and update the data in the service
            vm.saveData = function(newRow){
                //check if the id exist
                 if(DataService.checkIfIdExist(newRow.id)){
                    vm.data.push(newRow);
                    DataService.updateData(vm.data);
                    vm.newData = false;
                }  
            }

            vm.filterData = function(data){
               vm.data = DataService.getData();
               vm.data = $filter('filter')(vm.data,{'id':data});
           }

           // init the view when controller loaded
            function init(){
                vm.data = getDataFromService;
                if(angular.isUndefined(getState)){
                    vm.filterInput.id = "";
                }
                else{
                    vm.filterInput.id = getState.filterInput;
                    vm.data = $filter('filter')(vm.data,{'id':vm.filterInput.id});
                    //init selected row
                    if(!angular.isUndefined(getState.selectedRowIndex)){
                        vm.rowSelected = true;
                        selectedRowIndex = getState.selectedRowIndex;
                        vm.rowData =  vm.data[selectedRowIndex];
                    }
                    // if state left with new row to add
                    else if(!angular.isUndefined(getState.newData)){
                        vm.newData = true;
                    }
                }
            }

            //listen to state change from the data state and saves the state params
            $rootScope.$on('$stateChangeStart', 
                function(event, toState, toParams, fromState, fromParams, options){ 
                    if(fromState.name == 'data'){
                        if(vm.rowSelected)
                            current_state.selectedRowIndex = selectedRowIndex;
                        else if(vm.newData){
                            current_state.newData = true;
                        }
                        current_state.filterInput = vm.filterInput.id;
                        stateService.updateState(current_state);
                    }
                });

    }]);
})();

