'use strict';
// Declare app level module which depends on views, and components
angular.module('verifone', [
    'ui.router',
    'angularUtils.directives.dirPagination'
   
])
    .config(['$urlRouterProvider', '$stateProvider', '$httpProvider',
        function( $urlRouterProvider, $stateProvider, $httpProvider) {
            $stateProvider.
                state('layout', {
                    abstract: true,
                    templateUrl: "../partials/layout.html",
                    controller : 'layoutController as layoutCtrl'
                })
                .state('data', {
                    url: "/",
                    parent : 'layout',
                    controller:'dataController as dataCtrl',
                    resolve: {
                        getDataFromService : function(DataService){
                            return DataService.getData();
                        },
                        getState: function(stateService){
                            return stateService.getState();
                        }
                    },
                    templateUrl: "../partials/data.html"
                }).
                state('analysis', {
                    url: "/analysis",
                    parent : 'layout',
                    templateUrl: "../partials/analysis.html"
                }).
                state('monitor', {
                    url: "/monitor",
                    parent : 'layout',
                    templateUrl: "../partials/monitor.html"
                });
            $urlRouterProvider.otherwise('/');
        }]);

